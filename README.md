# devxblog-theme-kit

The tool for creating themes for [DevXBlog](https://devxblog.com)

## Prerequisites

- `python3.8` or newer

## Installing

```shell
pip install devxblog_theme_kit
```

## Releasing

```shell
bump2version patch|minor
./publish.sh
```




